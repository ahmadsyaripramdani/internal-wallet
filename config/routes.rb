Rails.application.routes.draw do
  namespace :api do
    resource :session, only: [:create, :destroy]
    resources :transactions, only: [:index, :create]
    resources :teams, only: [:show]
    resources :stocks, only: [:show]
    resources :users, only: [:show]
    get 'profile', to: "users#profile"
  end
end
