class CreateTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :transactions do |t|
      t.references :source_wallet, foreign_key: { to_table: :wallets }
      t.references :target_wallet, foreign_key: { to_table: :wallets }
      t.string :transaction_type
      t.decimal :amount, precision: 10, scale: 2

      t.timestamps
    end
  end
end
