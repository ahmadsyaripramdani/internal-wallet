# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

ahmad = User.create(
  email: 'ahmad@dot.com',
  password: 'test123'
)
ahmad.wallet.deposit(nil, ahmad.wallet, 5000)

other = User.create(
  email: 'other@dot.com',
  password: 'test123'
)
other.wallet.deposit(nil, other.wallet, 10000)

team = Team.create(
  name: 'A team'
)
Wallet.create(
  owner: team
)
team.wallet.deposit(nil, team.wallet, 25000)

stock = Stock.create(
  name: 'Fake BTC'
)
Wallet.create(
  owner: stock
)
stock.wallet.deposit(nil, stock.wallet, 30000)
