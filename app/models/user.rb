require 'securerandom'
require 'bcrypt'

class User < ApplicationRecord
  has_secure_password

  include BCrypt
  has_one :wallet, as: :owner
  validates :email, presence: true, uniqueness: true

  after_create :create_wallet

  def password
    @password ||= Password.new(password_digest)
  end

  def password=(new_password)
    @password = Password.create(new_password)
    self.password_digest = @password
  end

  def generate_api_key
    self.api_key = SecureRandom.hex(32)
    self.save!
  end

  private

  def create_wallet
    Wallet.create(owner: self)
  end
end
