class Wallet < ApplicationRecord
  belongs_to :owner, polymorphic: true
  has_many :transactions, dependent: :destroy

  def balance
    (Transaction.where(target_wallet: self).sum(:amount) || 0) - (Transaction.where(source_wallet: self).sum(:amount) || 0)
  end

  def deposit(source_wallet, target_wallet, amount)
    transaction = Transaction.new(
      source_wallet: source_wallet,
      target_wallet: target_wallet,
      amount: amount,
      transaction_type: 'deposit'
    )
    transaction.save
  end

  def withdraw(source_wallet, target_wallet, amount)
    transaction = Transaction.new(
      source_wallet: source_wallet,
      target_wallet: target_wallet,
      amount: amount,
      transaction_type: 'withdrawal'
    )
    transaction.save
  end
end
