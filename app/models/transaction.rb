class Transaction < ApplicationRecord
  belongs_to :source_wallet, class_name: 'Wallet', optional: true
  belongs_to :target_wallet, class_name: 'Wallet', optional: true

  validates :amount, presence: true, numericality: { greater_than: 0 }
  validate :validate_wallets

  private

  def validate_wallets
    errors.add(:base, 'Source or target wallet must be present') unless source_wallet.present? || target_wallet.present?
  end
end
