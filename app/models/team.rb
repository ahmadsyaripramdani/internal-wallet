class Team < ApplicationRecord
  has_one :wallet, as: :owner

  private

  def create_wallet
    Wallet.create(owner: self)
  end
end
