class Api::TeamsController < ApplicationController
  def show
    @team = Team.find_by(id: params[:id])

    if @team.nil?
      render json: { error: 'Team not found' }, status: :not_found
    else
      render json: {
        team: {
          name: @team.name,
          balance: @team.wallet.balance
        }
      },
      status: :ok
    end
  end
end
