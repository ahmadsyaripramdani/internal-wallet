class Api::StocksController < ApplicationController
  def show
    @stock = Stock.find_by(id: params[:id])
    if @stock.nil?
      render json: { error: 'Stock not found' }, status: :not_found
    else
      render json: {
        stock: {
          name: @stock.name,
          balance: @stock.wallet.balance
        }
      },
      status: :ok
    end
  end
end
