class Api::TransactionsController < ApplicationController

  def index
    @transactions = Transaction.where('source_wallet_id = :wallet_id OR target_wallet_id = :wallet_id', wallet_id: @current_user.wallet.id)
    render json: {
      transactions: @transactions
    }
  end

  def create
    amount = params[:amount].to_f
    current_wallet = @current_user.wallet
    against_wallet = Wallet.find_by(id: params[:wallet_id])

    case params[:transaction_type]
    when 'deposit'
      current_wallet.deposit(current_wallet, against_wallet, params[:amount])
      render json: { message: 'Deposit successful' }
    when 'withdrawal'
      current_wallet.withdraw(against_wallet, current_wallet, params[:amount])
      render json: { message: 'Withdrawal successful' }
    else
      render json: { error: 'Invalid transaction type' }, status: :unprocessable_entity
    end
  end
end
