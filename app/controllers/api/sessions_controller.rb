class Api::SessionsController < ApplicationController
  skip_before_action :authorize_request, only: :create

  def create
    user = User.find_by(email: params[:email])

    if user&.authenticate(params[:password])
      user.generate_api_key
      render json: {
        api_key: user.api_key,
        message: 'Logged in successfully.'
      }, status: :ok
    else
      render json: { message: 'Invalid email or password.' }, status: :unauthorized
    end
  end

  def destroy
    @current_user.generate_api_key
    render json: { message: 'Logged out successfully.' }, status: :ok
  end
end
