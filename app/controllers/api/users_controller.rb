class Api::UsersController < ApplicationController
  def show
    @user = User.find_by(id: params[:id])

    if @user.nil?
      render json: { error: 'User not found' }, status: :not_found
    else
      render json: {
        user: {
          email: @user.email,
          balance: @user.wallet.balance
        }
      },
      status: :ok
    end
  end

  def profile
    render json: {
      user: {
        email: @current_user.email,
        balance: @current_user.wallet.balance
      }
    },
    status: :ok
  end
end
