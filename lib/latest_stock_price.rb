module LatestStockPrice
  require 'net/http'
  require 'uri'
  require 'json'

  BASE_URL = 'https://latest-stock-price.p.rapidapi.com'

  def self.price(symbol)
    url = "#{BASE_URL}/price/#{symbol}"
    response = send_request(url)
    parse_response(response)
  end

  def self.prices(symbols)
    url = "#{BASE_URL}/prices?symbols=#{symbols.join(',')}"
    response = send_request(url)
    parse_response(response)
  end

  def self.price_all
    url = "#{BASE_URL}/price_all"
    response = send_request(url)
    parse_response(response)
  end

  private

  def self.send_request(url)
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    request = Net::HTTP::Get.new(uri.request_uri)
    request['X-RapidAPI-Key'] = ENV['RAPIDAPI_KEY']

    http.request(request)
  end

  def self.parse_response(response)
    JSON.parse(response.body)
  end
end
