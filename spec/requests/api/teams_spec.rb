# spec/controllers/api/teams_controller_spec.rb

require 'rails_helper'

RSpec.describe Api::TeamsController, type: :controller do
  describe 'GET #show' do
    context 'when the team exists' do
      let!(:team) { Team.create(name: 'Team A') }
      let!(:wallet) { Wallet.create(owner: team) }
      let!(:user) { User.create(email: 'test@example.com', password: 'password') }

      before do
        user.generate_api_key
        @request.headers['Authorization'] = "Bearer #{user.api_key}"
        allow_any_instance_of(Wallet).to receive(:balance).and_return(500)
        get :show, params: { id: team.id }
      end

      it 'returns the team information with balance' do
        expect(response).to have_http_status(:ok)
        expect(json_response).to eq({
          'team' => {
            'name' => 'Team A',
            'balance' => 500
          }
        })
      end
    end

    context 'when the team does not exist' do
      let!(:user) { User.create(email: 'test@example.com', password: 'password') }

      before do
        user.generate_api_key
        @request.headers['Authorization'] = "Bearer #{user.api_key}"
        get :show, params: { id: 123 }
      end

      it 'returns an error message' do
        expect(response).to have_http_status(:not_found)
        expect(json_response).to eq({ 'error' => 'Team not found' })
      end
    end
  end

  def json_response
    JSON.parse(response.body)
  end
end
