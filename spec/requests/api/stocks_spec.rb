# spec/controllers/api/stocks_controller_spec.rb

require 'rails_helper'

RSpec.describe Api::StocksController, type: :controller do
  describe 'GET #show' do
    context 'when the stock exists' do
      let!(:stock) { Stock.create(name: 'ABC') }
      let!(:wallet) { Wallet.create(owner: stock) }
      let!(:user) { User.create(email: 'test@example.com', password: 'password') }

      before do
        user.generate_api_key
        @request.headers['Authorization'] = "Bearer #{user.api_key}"
        allow_any_instance_of(Wallet).to receive(:balance).and_return(100)
        get :show, params: { id: stock.id }
      end

      it 'returns the stock information with balance' do
        expect(response).to have_http_status(:ok)
        expect(json_response).to eq({
          'stock' => {
            'name' => 'ABC',
            'balance' => 100
          }
        })
      end
    end

    context 'when the stock does not exist' do
      let!(:user) { User.create(email: 'test@example.com', password: 'password') }
      
      before do
        user.generate_api_key
        @request.headers['Authorization'] = "Bearer #{user.api_key}"
        get :show, params: { id: 123 }
      end

      it 'returns an error message' do
        expect(response).to have_http_status(:not_found)
        expect(json_response).to eq({ 'error' => 'Stock not found' })
      end
    end
  end

  def json_response
    JSON.parse(response.body)
  end
end
