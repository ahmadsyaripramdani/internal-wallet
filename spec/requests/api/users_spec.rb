# spec/controllers/api/users_controller_spec.rb

require 'rails_helper'

RSpec.describe Api::UsersController, type: :controller do
  describe 'GET #show' do
    context 'when the user exists' do
      let!(:user) { User.create(email: 'test@example.com', password: 'password') }
      let!(:wallet) { Wallet.create(owner: user) }

      before do
        user.generate_api_key
        @request.headers['Authorization'] = "Bearer #{user.api_key}"
        allow_any_instance_of(Wallet).to receive(:balance).and_return(1000)
        get :show, params: { id: user.id }
      end

      it 'returns the user information with balance' do
        expect(response).to have_http_status(:ok)
        expect(json_response).to eq({
          'user' => {
            'email' => 'test@example.com',
            'balance' => 1000
          }
        })
      end
    end

    context 'when the user does not exist' do
      let!(:user) { User.create(email: 'test@example.com', password: 'password') }

      before do
        user.generate_api_key
        @request.headers['Authorization'] = "Bearer #{user.api_key}"
        get :show, params: { id: 123 }
      end

      it 'returns an error message' do
        expect(response).to have_http_status(:not_found)
        expect(json_response).to eq({ 'error' => 'User not found' })
      end
    end
  end

  describe 'GET #profile' do
    context 'when the user is authenticated' do
      let!(:user) { User.create(email: 'test@example.com', password: 'password') }
      let!(:wallet) { Wallet.create(owner: user) }

      before do
        user.generate_api_key
        @request.headers['Authorization'] = "Bearer #{user.api_key}"
        allow_any_instance_of(Wallet).to receive(:balance).and_return(2000)
        get :profile
      end

      it 'returns the authenticated user information with balance' do
        expect(response).to have_http_status(:ok)
        expect(json_response).to eq({
          'user' => {
            'email' => 'test@example.com',
            'balance' => 2000
          }
        })
      end
    end

    context 'when the user is not authenticated' do
      before do
        get :profile
      end

      it 'returns an unauthorized response' do
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  def json_response
    JSON.parse(response.body)
  end
end
