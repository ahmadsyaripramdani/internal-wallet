# spec/controllers/api/transactions_controller_spec.rb

require 'rails_helper'

RSpec.describe Api::TransactionsController, type: :controller do
  describe 'GET #index' do
    context 'when the user is authenticated' do
      let!(:user) { User.create(email: 'test@example.com', password: 'password') }
      let!(:wallet) { Wallet.create(owner: user) }
      let!(:transaction1) { Transaction.create(source_wallet: wallet, target_wallet: wallet, amount: 100, transaction_type: 'deposit') }
      let!(:transaction2) { Transaction.create(source_wallet: wallet, target_wallet: wallet, amount: 50, transaction_type: 'withdrawal') }

      before do
        user.generate_api_key
        @request.headers['Authorization'] = "Bearer #{user.api_key}"
        get :index
      end

      it 'returns the user transactions' do
        expect(response).to have_http_status(:ok)
        # expect(json_response['transactions']).to eq([
        #   transaction1.as_json,
        #   transaction2.as_json
        # ])
      end
    end

    context 'when the user is not authenticated' do
      before do
        get :index
      end

      it 'returns an unauthorized response' do
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe 'POST #create' do
    let!(:user) { User.create(email: 'test@example.com', password: 'password') }
    let!(:current_wallet) { Wallet.create(owner: user) }
    let!(:against_wallet) { Wallet.create }

    context 'when the user is authenticated' do
      before do
        user.generate_api_key
        @request.headers['Authorization'] = "Bearer #{user.api_key}"
      end

      context 'with valid parameters' do
        it 'creates a deposit transaction' do
          post :create, params: {
            amount: 100,
            wallet_id: against_wallet.id,
            transaction_type: 'deposit'
          }

          expect(response).to have_http_status(:ok)
          expect(json_response).to eq({ 'message' => 'Deposit successful' })
          expect(Transaction.last.transaction_type).to eq('deposit')
        end

        it 'creates a withdrawal transaction' do
          post :create, params: {
            amount: 50,
            wallet_id: against_wallet.id,
            transaction_type: 'withdrawal'
          }

          expect(response).to have_http_status(:ok)
          expect(json_response).to eq({ 'message' => 'Withdrawal successful' })
          expect(Transaction.last.transaction_type).to eq('withdrawal')
        end
      end

      context 'with invalid parameters' do
        it 'returns an error message' do
          post :create, params: {
            amount: 100,
            wallet_id: against_wallet.id,
            transaction_type: 'invalid'
          }

          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response).to eq({ 'error' => 'Invalid transaction type' })
        end
      end
    end

    context 'when the user is not authenticated' do
      before do
        post :create, params: {
          amount: 100,
          wallet_id: against_wallet.id,
          transaction_type: 'deposit'
        }
      end

      it 'returns an unauthorized response' do
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  def json_response
    JSON.parse(response.body)
  end
end
