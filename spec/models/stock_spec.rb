# spec/models/stock_spec.rb

require 'rails_helper'

RSpec.describe Stock, type: :model do
  describe "associations" do
    it { should have_one(:wallet) }
  end
  
  describe "private methods" do
    let(:stock) { Stock.new }

    describe "#create_wallet" do
      it "creates a new wallet with the stock as the owner" do
        expect {
          stock.send(:create_wallet)
        }.to change(Wallet, :count).by(1)

        expect(stock.wallet).to be_present
        expect(stock.wallet.owner).to eq(stock)
      end
    end
  end
end
