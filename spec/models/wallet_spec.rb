# spec/models/wallet_spec.rb

require 'rails_helper'

RSpec.describe Wallet, type: :model do
  describe "#balance" do
    let(:owner) { User.create(email: 'a@b.com', password: 'test123')}
    let(:wallet) { Wallet.create(owner: owner) }

    it "returns the balance of the wallet" do
      transaction1 = Transaction.create(target_wallet: wallet, amount: 100)
      transaction2 = Transaction.create(source_wallet: wallet, amount: 50)
      transaction3 = Transaction.create(source_wallet: wallet, amount: 25)

      expect(wallet.balance).to eq(100 - 50 - 25)
    end

    it "returns 0 if there are no transactions" do
      expect(wallet.balance).to eq(0)
    end
  end

  describe "#deposit" do
    let(:owner) { User.create(email: 'a@b.com', password: 'test123')}
    let(:team) { Team.create(name: 'team a') }
    let(:wallet) { Wallet.create(owner: owner) }
    let(:source_wallet) { Wallet.create(owner: team) }
    let(:amount) { 100 }

    it "creates a deposit transaction" do
      expect {
        wallet.deposit(source_wallet, wallet, amount)
      }.to change(Transaction, :count).by(1)

      transaction = Transaction.last
      expect(transaction.source_wallet).to eq(source_wallet)
      expect(transaction.target_wallet).to eq(wallet)
      expect(transaction.amount).to eq(amount)
      expect(transaction.transaction_type).to eq("deposit")
    end
  end

  describe "#withdraw" do
    let(:owner) { User.create(email: 'a@b.com', password: 'test123')}
    let(:team) { Team.create(name: 'team a') }
    let(:wallet) { Wallet.create(owner: owner) }
    let(:source_wallet) { Wallet.create(owner: team) }
    let(:amount) { 50 }

    it "creates a withdrawal transaction" do
      expect {
        wallet.withdraw(source_wallet, wallet, amount)
      }.to change(Transaction, :count).by(1)

      transaction = Transaction.last
      expect(transaction.source_wallet).to eq(source_wallet)
      expect(transaction.target_wallet).to eq(wallet)
      expect(transaction.amount).to eq(amount)
      expect(transaction.transaction_type).to eq("withdrawal")
    end
  end
end
