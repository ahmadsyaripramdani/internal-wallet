# spec/models/team_spec.rb

require 'rails_helper'

RSpec.describe Team, type: :model do
  describe "associations" do
    it { should have_one(:wallet) }
  end
  
  describe "private methods" do
    let(:team) { Team.new }

    describe "#create_wallet" do
      it "creates a new wallet with the team as the owner" do
        expect {
          team.send(:create_wallet)
        }.to change(Wallet, :count).by(1)

        expect(team.wallet).to be_present
        expect(team.wallet.owner).to eq(team)
      end
    end
  end
end
