# spec/models/user_spec.rb

require 'rails_helper'

RSpec.describe User, type: :model do
  describe "associations" do
    it { should have_one(:wallet) }
  end

  # describe "validations" do
  #   it { should validate_presence_of(:email) }
  #   it { should validate_uniqueness_of(:email) }
  # end

  describe "password encryption" do
    let(:user) { User.create(email: "test@example.com", password: "password") }

    it "encrypts the password" do
      expect(user.password_digest).not_to be_nil
    end

    it "authenticates the user with the correct password" do
      expect(user.authenticate("password")).to eq(user)
    end

    it "does not authenticate the user with an incorrect password" do
      expect(user.authenticate("wrong_password")).to be_falsey
    end
  end

  describe "#generate_api_key" do
    let(:user) { User.create(email: "test@example.com", password: "password") }

    it "generates a unique API key for the user" do
      expect {
        user.generate_api_key
      }.to change(user, :api_key).from(nil)
      expect(user.api_key).to be_present
    end
  end

  describe "callbacks" do
    it "creates a wallet after user creation" do
      user = User.create(email: "test@example.com", password: "password")
      expect(user.wallet).to be_present
      expect(user.wallet.owner).to eq(user)
    end
  end
end
