# spec/models/transaction_spec.rb

require 'rails_helper'

RSpec.describe Transaction, type: :model do
  describe "associations" do
    it { should belong_to(:source_wallet).class_name('Wallet').optional(true) }
    it { should belong_to(:target_wallet).class_name('Wallet').optional(true) }
  end

  describe "validations" do
    it { should validate_presence_of(:amount) }
    it { should validate_numericality_of(:amount).is_greater_than(0) }

    context "when source or target wallet is not present" do
      let(:transaction) { Transaction.new }

      it "is invalid" do
        transaction.valid?
        expect(transaction.errors[:base]).to include('Source or target wallet must be present')
      end
    end

    context "when source wallet is present" do
      let(:source_wallet) { Wallet.create }
      let(:transaction) { Transaction.new(source_wallet: source_wallet, amount: 100) }

      it "is valid" do
        expect(transaction).to be_valid
      end
    end

    context "when target wallet is present" do
      let(:target_wallet) { Wallet.create }
      let(:transaction) { Transaction.new(target_wallet: target_wallet, amount: 100) }

      it "is valid" do
        expect(transaction).to be_valid
      end
    end
  end
end
