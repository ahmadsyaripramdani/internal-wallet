
# Internal wallet transactional system (API)




## Installation

clone the project

```bash
  cd project-path
  rake db:create && rake db:migrate
  rake db:seed
```

## Demo


`/api/login`
```  
  email: 'ahmad@dot.com',
  password: 'test123'
```

## Test

`rspec spec`

## Documentation

[Documentation API](https://documenter.getpostman.com/view/14589370/2s93saZY1C)


## Authors

- Ahmad Ramdani
